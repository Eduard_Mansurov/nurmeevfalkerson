import java.util.ArrayList;

/**
 * Created by EduardL on 25.04.15.
 */
public class Helper {
    //Проверяет все дуги из начальной вершины
    public static int flowsBegin(ArrayList<Arc> arcs) {
        for (int i = 0; i < arcs.size(); i++) {
            if (arcs.get(i).getBegin() == 0 && arcs.get(i).getFlow() == 0) {
                return i;
            }
        }
        return -1;
    }

    //Возвращает ребро с заданным началом, на которое можно перейти
    public static Integer arcWithBegin(int begin, ArrayList<Arc> arcs) {
        for (int i = 0; i < arcs.size(); i++) {
            if (arcs.get(i).getBegin() == begin && arcs.get(i).getFlow() == 0 && arcs.get(i).getBegin() != 0) {
                return i;
            } else if (arcs.get(i).getEnd() == begin && arcs.get(i).getFlow() == 1 && arcs.get(i).getBegin() != 0) {
                return -i;
            }
        }
        return null;
    }
}
