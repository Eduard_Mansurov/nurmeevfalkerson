import java.util.ArrayList;

/**
 * Created by EduardL on 23.04.15.
 */
public class Main {
    public static void main(String[] args) {
//        int[][] adjacencyMatrix = new int[][]{{0, 1, 1, 1, 0, 0, 0},
//                {0, 0, 0, 0, 1, 0, 0},
//                {0, 0, 0, 0, 1, 1, 0},
//                {0, 0, 0, 0, 0, 1, 0},
//                {0, -1, -1, 0, 0, 0, 1},
//                {0, 0, -1, -1, 0, 0, 1},
//                {0, 0, 0, 0, -1, -1, 0}};

        int[][] adjacencyMatrix = new int[][]{{0,1,1,1,1,0,0,0,0},
                {0,0,0,0,0,1,0,0,0},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,1,1,1,0},
                {0,-1,0,0,-1,0,0,0,1},
                {0,0,-1,-1,-1,0,0,0,1},
                {0,0,0,0,-1,0,0,0,1},
                {0,0,0,0,0,-1,-1,-1,0}};


        ArrayList<Arc> arcs = new ArrayList<Arc>();

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] == 1) {
                    arcs.add(new Arc(i, j));
                }
            }
        }

        int i=0;
        while (Helper.flowsBegin(arcs) != -1) {
            i = Helper.flowsBegin((arcs));
            while (Helper.arcWithBegin(arcs.get(i).getEnd(), arcs) != null) {
                if (Helper.arcWithBegin(arcs.get(i).getEnd(), arcs) > 0) {
                    arcs.get(i).setFlow(1);
                } else {
                    arcs.get(i).setFlow(0);
                }
                i++;
            }
        }

        for (int j = 0; j < arcs.size(); j++) {
            if (arcs.get(j).getFlow() == 1) {
                System.out.println(arcs.get(j).getBegin() + " " + arcs.get(j).getEnd());
            }
        }
    }
}
