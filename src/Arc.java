/**
 * Created by EduardL on 23.04.15.
 */
public class Arc {
    int flow = 0;
    int begin;
    int end;

    public Arc(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    public int getFlow() {
        return flow;
    }

    public void setFlow(int flow) {
        this.flow = flow;
    }

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }
}
